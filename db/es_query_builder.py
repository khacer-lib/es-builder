from enum import Enum
from typing import Any
import json


class OrderType(Enum):
    DECS = "desc"
    ASC = "asc"


class CompareOperator(Enum):
    GT = "gt"
    GTE = "gte"
    LT = "lt"
    LTE = "lte"


class QueryESBuilder:
    def __init__(self):
        self.__typequery = ''

    def build_search(self):
        self.__typequery = "_search"
        return QueryESSearch()

    # build more query here


class BaseQuery:
    def __init__(self):
        self.query = {}

    def get_query(self, debug=False):
        if debug:
            print(json.dumps(self.query, indent=2))
        return self.query


class QueryESSearch(BaseQuery):
    def __init__(self):
        super().__init__()

    def start_query(self):
        self.query["query"] = {}
        return self.Query(self)

    def start_aggs(self):
        if "aggs" not in self.query:
            self.query["aggs"] = {}
        return self.Aggs(self)

    def add_pagination(self, _size: int = 5, _from: int = 0):
        self.query['size'] = _size
        self.query['from'] = _from
        return self

    def add_sort(self, field: str, order: OrderType = OrderType.ASC):
        self.query["sort"] = {
            field: {
                "order": order.value
            }
        }
        return self

    class Query:
        def __init__(self, instance):
            self.instance = instance
            self.query = instance.query["query"]

        def match(self, field: str, value: Any):
            self.query["match"] = {field: value}

        def term(self, field: str, value: Any):
            self.query["terms"] = {field: value}

        def range(self, field: str, compare1: CompareOperator, value1, compare2: CompareOperator, value2):
            self.query["range"] = {
                field: {
                    compare1.value: value1,
                    compare2.value: value2
                }
            }

        def should(self):
            self.query["bool"] = {"should": []}
            return ShouldQuery(self)

        def must(self):
            self.query["bool"] = {"must": []}
            return MustQuery(self)

    class Aggs:
        def __init__(self, instance):
            self.instance = instance
            self.query = instance.query

        def cardinality(self, alias: str, field: str):
            self.query["aggs"][alias] = {"cardinality": {"field": field}}
            return self

        def add_sum_bucket(self, alias: str, bucket_path: str):
            self.query["aggs"][alias] = {"sum_bucket": {"buckets_path": bucket_path}}
            return self

        def top_hits(self, alias: str, field: [], _size: int = 1, sort_field: str = None,
                     order: OrderType = OrderType.ASC):
            self.query["aggs"][alias] = {
                "top_hits": {
                    "_source": {"includes": field},
                    "size": _size
                }
            }
            if sort_field is not None:
                self.query["aggs"][alias]["top_hits"]["sort"] = [
                    {sort_field: {"order": order.value}}]
            return self

        def group_by(self, alias: str, field: str = None, script_source: str = None, _size: int = None,
                     _from: int = None, order: OrderType = None):
            if field is not None:
                self.query["aggs"][alias] = {"terms": {"field": field}}
            if script_source is not None:
                self.query["aggs"][alias] = {"terms": {"script": {"source": script_source}}}
            if _size is not None:
                self.query["aggs"][alias]["terms"]["size"] = _size
            # if _from is not None:
            #     self.query["aggs"][alias]["terms"]["from"] = _from
            if order is not None:
                self.query["aggs"][alias]["terms"]["order"] = {"_key": order.value}
            return self.Term(self, alias)

        def group_by_and_aggs(self, alias: str, field: str, _size: int = None, script_source: str = None,
                              _from: int = None, order: OrderType = None):
            if field is not None:
                self.query["aggs"][alias] = {"terms": {"field": field}}
            if script_source is not None:
                self.query["aggs"][alias] = {"terms": {"script": {"source": script_source}}}
            if _size is not None:
                self.query["aggs"][alias]["terms"]["size"] = _size
            if _from is not None:
                self.query["aggs"][alias]["terms"]["from"] = _size
            if order is not None:
                self.query["aggs"][alias]["terms"]["order"] = {"_key": order.value}
            self.query = self.query["aggs"][alias]
            self.query["aggs"] = {}
            return self

        class Term:
            def __init__(self, instance, alias):
                self.alias_father = alias
                self.instance = instance.instance
                self.query = instance.query

            def cardinality(self, alias: str, field: str):
                if "aggs" in self.query["aggs"][self.alias_father]:
                    self.query["aggs"][self.alias_father]["aggs"][alias] = {"cardinality": {"field": field}}
                else:
                    self.query["aggs"][self.alias_father]["aggs"] = {alias: {"cardinality": {"field": field}}}
                return self

            def top_hits(self, alias: str, field: [], _size: int = 1, sort_field: str = None,
                         order: OrderType = OrderType.ASC):
                if "aggs" in self.query["aggs"][self.alias_father]:
                    self.query["aggs"][self.alias_father]["aggs"][alias] = {
                        "top_hits": {
                            "_source": {"includes": field},
                            "size": _size
                        }
                    }
                else:
                    self.query["aggs"][self.alias_father]["aggs"] = {
                        alias: {
                            "top_hits": {
                                "_source": {"includes": field},
                                "size": _size
                            }
                        }
                    }
                if sort_field is not None:
                    self.query["aggs"][self.alias_father]["aggs"][alias]["top_hits"]["sort"] = [{sort_field: {"order": order.value}}]
                return self


class MustQuery:
    def __init__(self, instance):
        self.instance = instance
        self.query = instance.query

    def match(self, field: str, value: Any):
        self.query["bool"]["must"].append({"match": {field: value}})
        return self

    def terms(self, field: str, value: Any):
        self.query["bool"]["must"].append({"terms": {field: value}})
        return self

    def range(self, field: str, compare1: CompareOperator, value1, compare2: CompareOperator, value2):
        self.query["bool"]["must"].append({"range": {
            field: {
                compare1.value: value1,
                compare2.value: value2
            }
        }})
        return self

    def should(self):
        self.query["bool"]["must"].append({"bool": {"should": []}})
        for x in self.query["bool"]["must"]:
            if "bool" in x:
                self.query = x
        return ShouldQuery(self)


class ShouldQuery:
    def __init__(self, instance):
        self.instance = instance
        self.query = instance.query

    def terms(self, field: str, values: []):
        self.query["bool"]["should"].append({"terms": {field: values}})
        return self


if __name__ == "__main__":
    search = QueryESBuilder().build_search().add_pagination(1, 0).add_sort("timestamp")
    query = search.start_query()
    # # query.match("log_source_name", "test")
    # # query.range("timestamp", CompareOperator.GTE, 1690280051639, CompareOperator.LTE, 1690280051639)
    query.must().match("log_source_name", "test").range("timestamp", CompareOperator.GTE, 1690280051639,
                                                      CompareOperator.LTE, 1690280051639) \
        .should().terms("geoip.country_name.keyword", ["Vietnam"])

    # aggs = search.start_aggs()
    # scrip = """int ix = doc['dest_ip.keyword'].getValue().indexOf('.');
    #           return doc['dest_ip.keyword'].getValue().substring(0,ix);"""
    # aggs.group_by_and_aggs("group_sen", "severity").group_by("group_ip", script_source=scrip).top_hits("top_hit",
    #                                                                                                    ["vendor_name",
    #                                                                                                     "vendor_short_name"])
    # aggs.group_by("parent_organ_category", "parent_organ_category.keyword", 3, order=OrderType.ASC).cardinality(
    #     "unit_parent_id", "unit_parent_id.keyword").top_hits("top_hit", ["vendor_name", "vendor_short_name"])
    # aggs = search.start_aggs()
    # aggs.add_sum_bucket("total_parent_organ_category", "parent_organ_category>unit_parent_id")
    json_formatted_str = json.dumps(search.get_query(), indent=2)

    print(search.get_query())
    print(json_formatted_str)
