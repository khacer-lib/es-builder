from app.core.config import settings
import requests
from fastapi import HTTPException
from datetime import timedelta, datetime


class ES:
    def __init__(self):
        self._request_headers = {'content-type': 'application/json'}
        self._prefix_indices = settings.ES_INDICES

    @staticmethod
    def __get_url(indices):
        return "".join([settings.ES_URL, "/{}/_search".format(indices)])

    def get_indices_all(self):
        return "".join([self._prefix_indices, "*"])

    def get_indices(self, _form: int, _to: int):
        if _form > _to:
            raise HTTPException(
                status_code=404,
                detail="_form must less than _to",
            )

        try:
            from_date = datetime.fromtimestamp(_form)
            # to_date = datetime.fromtimestamp(_to)
            time_diff = timedelta(seconds=_to - _form)
        except:
            from_date = datetime.fromtimestamp(_form / 1000)
            # to_date = datetime.fromtimestamp(_to / 1000)
            time_diff = timedelta(seconds=_to / 1000 - _form / 1000)

        days = time_diff.days + 1
        if (time_diff - timedelta(time_diff.days)).seconds > 0:
            days = time_diff.days + 2

        if time_diff.days == 0:
            return "".join([self._prefix_indices, from_date.strftime("-%Y.%m.%d-"), "*"])
        else:
            return ",".join(["".join([self._prefix_indices, x.strftime("-%Y.%m.%d-"), "*"]) for x in
                             (from_date + timedelta(n) for n in range(days))])

    def search(self, query, indices):
        try:
            print("url", self.__get_url(indices))
            res = requests.get(url=self.__get_url(indices), json=query, headers=self._request_headers)
            if not res.ok:
                raise HTTPException(
                    status_code=400,
                    detail="Not Found",
                )
            else:
                return res.json()
        except requests.exceptions.ConnectionError:
            raise HTTPException(
                status_code=500,
                detail="Can't connect ES",
            )
        except Exception as e:
            print(e)
            raise HTTPException(
                status_code=500,
                detail="Something Wrong!",
            )
